import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, Platform, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { PetServiceProvider } from "../../providers/pet-service/pet-service";
@IonicPage()
@Component({
  selector: 'page-create-denunciation',
  templateUrl: 'create-denunciation.html',
})
export class CreateDenunciationPage {

  titulo:string = "";
  descripcion:string = "";
  ubicacion:string = "";
  imgPreview:string = null;
  img:string = "";

  myForm: FormGroup;

  constructor(public formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private platform: Platform,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private imagePicker: ImagePicker,
    private camera: Camera,
    private _petCtrl: PetServiceProvider) {
  }
  back() {
    this.navCtrl.setRoot('StartPage')
  }
  atras() {
    this.navCtrl.setRoot('StartPage');
  }
  crear_publicacion(){
    //console.log(this.myForm.value.titulo);
    //console.log("Subiendo imagen...");
    var date_d = new Date();
    var date = date_d.getFullYear()+'-'+(date_d.getMonth()+1)+'-'+(date_d.getDate())+' '+date_d.getHours()+':'+date_d.getMinutes()+':'+date_d.getSeconds();
    let archivo = {
      'titulo': this.titulo,
      'descripcion': this.descripcion,
      'ubicacion': this.ubicacion,
      'date': date,
      'img': this.img
    };
    console.log("let archivo ");
    console.log(archivo)

    let loader = this.loadingCtrl.create({
      content: "Subiendo..."
    });
    loader.present();
    
    this._petCtrl.cargar_denuncia( archivo ) 
          .then(
            ()=>{
              loader.dismiss();
              this.atras()
            },

            ( error )=>{
              loader.dismiss();
              this.mostrar_toast("Error al cargar: " + error );
              console.log("Error al cargar " + JSON.stringify(error) );
            }

           )


  }
  private mostrar_toast( texto:string ){

    this.toastCtrl.create({
      message: texto,
      duration: 2500
    }).present();

  }

  mostrar_camara(){

    if( !this.platform.is("cordova") ){
      this.mostrar_toast("Error: No estamos en un celular");
      return;
    }


    const options: CameraOptions = {
        quality: 40,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
       // imageData is either a base64 encoded string or a file URI
       // If it's base64:
       this.imgPreview = 'data:image/jpeg;base64,' + imageData;
       this.img = imageData;

      }, (err) => {
       // Handle error
       this.mostrar_toast( "Error: " + err );
       console.error("Error en la camara: ", err);

    });


  }

  seleccionar_fotos(){

    if( !this.platform.is("cordova") ){
      this.mostrar_toast("Error: No estamos en un celular");
      return;
    }

    let opciones: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 40,
      outputType: 1
    }


    this.imagePicker.getPictures(opciones).then((results) => {


      for( let img of results ){
        this.imgPreview = 'data:image/jpeg;base64,' + img
        this.img = img;
        break;
      }


    }, (err) => {

      this.mostrar_toast("Error seleccion:" + err);
      console.error(  "Error en seleccion: " + JSON.stringify( err ) );

    });

  }
}
