import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateDenunciationPage } from './create-denunciation';

@NgModule({
  declarations: [
    CreateDenunciationPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateDenunciationPage),
  ],
})
export class CreateDenunciationPageModule {}
