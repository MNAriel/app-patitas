import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReadPublicationPage } from './read-publication';

@NgModule({
  declarations: [
    ReadPublicationPage,
  ],
  imports: [
    IonicPageModule.forChild(ReadPublicationPage),
  ],
})
export class ReadPublicationPageModule {}
