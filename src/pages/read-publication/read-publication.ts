import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DatosService} from '../../service/datos.service'



@IonicPage()
@Component({
  selector: 'page-read-publication',
  templateUrl: 'read-publication.html',
})
export class ReadPublicationPage {
  datos = [];
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dService: DatosService){
        this.datos = this.dService.getDatos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReadPublicationPage');
  }

}
