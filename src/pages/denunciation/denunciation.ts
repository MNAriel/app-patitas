import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-denunciation',
  templateUrl: 'denunciation.html',
})
export class DenunciationPage {
  dato:any = [];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private socialSharing: SocialSharing,
              private toastCtrl: ToastController) {
    this.dato = this.navParams.get('dato');
    console.log(this.dato)
  }
  back() {
    this.navCtrl.pop();
  }
  compartir( dato:any ) {
  let message = dato.titulo + " \n " + dato.descripcion + "\n" + "Referencia : " + dato.name +" "+ dato.phone;
  let subjet = dato.descp + "\n" + "Referencia : " + dato.name +" "+ dato.phone;
  let file = dato.img;

    this.socialSharing.share(message, subjet, file, null).then(() => {
      this.toastCtrl.create({
        message: "Compartodo exitosamente",
        duration: 2500
      }).present();  
    }).catch(( error ) => {
      this.toastCtrl.create({
        message: error,
        duration: 2500
      }).present(); 
    });
  }

  
}