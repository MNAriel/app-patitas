import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DenunciationPage } from './denunciation';

@NgModule({
  declarations: [
    DenunciationPage,
  ],
  imports: [
    IonicPageModule.forChild(DenunciationPage),
  ],
})
export class DenunciationPageModule {}
