import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {
  listaAyuda = [];
   
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.listaAyuda =[
      {'titulo':"Como usar PATITAS",'icono':"paw",
      'descripcion':"Con PATITAS: usted tiene la opcion de publicar un extravio,adopcion o una denucia, puede encontrar publicaciones que respecten a un tipo de publicacion"},
      {'titulo':"Inicio de sesion ",'icono':"contact",
      'descripcion':"el acceso a la app PATITAS, puede realizarse desde una cuenta de facebook o gmail"},
      {'titulo':"Cambiar de cuenta",'icono':"md-people",
      'descripcion':""},
      {'titulo':"Contactanos",'icono':"ios-contacts",
      'descripcion':"Abdiel Joel Orellana abdielorellana3@gmail.com"},     
    ]
  }
  openNavDetailsPage(item){
  this.navCtrl.push('InfoAyudaPage', { item: item});    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

}
