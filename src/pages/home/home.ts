import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DatosService } from '../../service/datos.service';
import { PetServiceProvider } from '../../providers/pet-service/pet-service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  publaciones:any = [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public dService: DatosService,
              public modalCtrl: ModalController,
              public _petCtrl: PetServiceProvider) {
              this._petCtrl.getPublicacionAdopcion().valueChanges().subscribe( test => {
                    console.log(test)
                    //return test;
                    this.publaciones = test;
                    console.log(this.publaciones)
                    
                  })              
                  //console.log(this.publaciones)        
  }

  // irPublicacion( dato:any ) {
  //   console.log(dato)
  //   this.navCtrl.push( 'PublicationPage', { 'dato':dato } );
  // }
  presentModal(dato:any ) {
    const modal = this.modalCtrl.create('PublicationPage', { 'dato':dato });
    modal.present();
  }
}
