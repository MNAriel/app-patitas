import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateAdoptionPage } from './create-adoption';

@NgModule({
  declarations: [
    CreateAdoptionPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateAdoptionPage),
  ],
})
export class CreateAdoptionPageModule {}
