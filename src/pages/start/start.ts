import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html'
})
export class StartPage {

  extravioRoot = 'ExtravioPage'
  denunciasRoot = 'DenunciasPage'
  adopcionesRoot = 'AdopcionesPage'


  constructor(public navCtrl: NavController) {}

}
