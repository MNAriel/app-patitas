import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { PetServiceProvider } from '../../providers/pet-service/pet-service';
import { DatosService } from '../../service/datos.service';

@IonicPage()
@Component({
  selector: 'page-adopciones',
  templateUrl: 'adopciones.html',
})
export class AdopcionesPage {

  datos = [];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public dService: DatosService,
              public _petCtrl: PetServiceProvider,
              public modalCtrl: ModalController) {
          this.datos = this.dService.getDatos();
          this._petCtrl.getPublicacionAdopcion().valueChanges().subscribe( test => {
            console.log(test)
            //return test;
            this.datos = test;
            console.log(this.datos)
            
          })
  }
  presentModal(dato:any ) {
    const modal = this.modalCtrl.create('PublicationPage', { 'dato':dato });
    modal.present();
  }
}