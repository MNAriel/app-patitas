import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoAyudaPage } from './info-ayuda';

@NgModule({
  declarations: [
    InfoAyudaPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoAyudaPage),
  ],
})
export class InfoAyudaPageModule {}
