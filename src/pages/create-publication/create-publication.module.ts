import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatePublicationPage } from './create-publication';

@NgModule({
  declarations: [
    CreatePublicationPage,
  ],
  imports: [
    IonicPageModule.forChild(CreatePublicationPage),
  ],
})
export class CreatePublicationPageModule {}
