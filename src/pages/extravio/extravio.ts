import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { DatosService } from '../../service/datos.service';
import { PetServiceProvider } from '../../providers/pet-service/pet-service';

@IonicPage()
@Component({
  selector: 'page-extravio',
  templateUrl: 'extravio.html',
})
export class ExtravioPage {
  
  datos = [];
  //publaciones:any = [];
//
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public dService: DatosService,
              public _petCtrl: PetServiceProvider,  
              public modalCtrl: ModalController) {
    //this.datos = this.dService.getDatos();
    this._petCtrl.getPublicacionExtravio().valueChanges().subscribe( test => {
                    console.log(test)
                    //return test;
                    this.datos = test;
                    console.log(this.datos)
                    
                  }) 
  }

  presentModal(dato:any ) {
    const modal = this.modalCtrl.create('PublicationPage', { 'dato':dato });
    modal.present();
  }

}
 