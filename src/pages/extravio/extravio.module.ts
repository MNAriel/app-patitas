import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExtravioPage } from './extravio';

@NgModule({
  declarations: [
    ExtravioPage,
  ],
  imports: [
    IonicPageModule.forChild(ExtravioPage),
  ],
})
export class ExtravioPageModule {}
