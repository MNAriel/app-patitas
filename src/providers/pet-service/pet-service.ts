import { Injectable } from '@angular/core';

import { ToastController } from "ionic-angular";

import * as firebase from "firebase";
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFireModule } from '@angular/fire';

@Injectable()
export class PetServiceProvider {

  private CARPETA_IMAGENES_ADOPCION:string = "img/adopcion";
  private POSTS_A:string = "patitas-bedb7/adopcion";

  private CARPETA_IMAGENES_EXTRAVIO:string = "img/extravio  ";
  private POSTS_D:string = "patitas-bedb7/extravio";

  private CARPETA_IMAGENES_DENUCIA:string = "img/denuncia  ";
  private POSTS_DENUCUA:string = "patitas-bedb7/denuncia";

  imagenes:any[] = [];
  lastKey:string = undefined;
  publicacion:any = [];



  constructor(public af: AngularFireDatabase,
              private toastCtrl: ToastController
              ) {}
  // getPublication() {
  //   return this.af.list(`/${ this.POSTS }`).valueChanges();
  // } 
  //obtener las publicaciones de adopciones en firebase
  getPublicacionAdopcion() {
    console.log('llega')
    return this.af.list('/patitas-bedb7/adopcion');
  }

  getPublicacionExtravio() {
    console.log('llega')
    return this.af.list('/patitas-bedb7/extravio');
  }

  getPublicacionDenuncia() {
    console.log('llega')
    return this.af.list('/patitas-bedb7/denuncia');
  }
  // getPublicacion(descripcion:string) {
  //   console.log('llega')
  //   return this.af.list('/patitas-bedb7/', 
  //     ref => ref.orderByChild('descripcion').equalTo(descripcion));
  // }
  // getPublicacion() {
  //   console.log('llega')
  //   this.af.object('patitas-bedb7/').valueChanges().subscribe( test => {
  //     console.log(test)
  //     this.publicacion = test;
  //     console.log(test)
  //     return this.publicacion;  
  //   })
  // }



  cargar_imagenes_firebase( archivo:archivoSubir ){
    
    let promesa = new Promise( (resolve, reject)=>{

      this.mostar_toast("Inicio de carga");
      let storageRef = firebase.storage().ref();
      let nombreArchivo = new Date().valueOf(); //1237128371
      
      let uploadTask:firebase.storage.UploadTask =
              storageRef.child(`${ this.CARPETA_IMAGENES_ADOPCION  }/${ nombreArchivo }`)
              .putString( archivo.img, 'base64', { contentType: 'image/jpeg' }  );

                    
      uploadTask.on(  firebase.storage.TaskEvent.STATE_CHANGED,
          ( snapshot )=>{}, // saber el avance del archivo
          ( error )=> {  // Manejo de errores

            console.log("Error al subir ", JSON.stringify( error ));
            this.mostar_toast("Error al cargar: " + JSON.stringify( error ) );
            reject(error);

          },
          ()=>{ // Termino el proceso
            
            //let url = uploadTask.snapshot.downloadURL;
            // let url = uploadTask.snapshot.ref.getDownloadURL().then(function (URL){
            //   // console.log('File available at');
            //   // console.log(URL)
            //   return URL
            // })
            uploadTask.then((snapshot) => {
                snapshot.ref.getDownloadURL().then((url) => {
                    // do something with url here
                    //console.log(url)
                    let URL =  url
                    console.log(URL)
                    this.crear_post( URL, 
                                      archivo.titulo, 
                                      archivo.descripcion, 
                                      archivo.mascota_name,
                                      archivo.name,
                                      archivo.phone
                                      );
                });
            });
            
            this.mostar_toast("Imagen cargada exitosamente!!");
            // this.crear_post( URL, 
            //                  archivo.titulo, 
            //                  archivo.descripcion, 
            //                  archivo.mascota_name,
            //                  archivo.name,
            //                  archivo.phone
            //                 );
            
            resolve();

          }
        )

    });



    return promesa;

  }


  private crear_post( url:any,
                      titulo:string, 
                      descripcion:string, 
                      mascota_name:string, 
                      name:string, 
                      phone:string
                     ){
    console.log("llego hasta aqui")                    
    let post:archivoSubir = {
      img: url,
      titulo: titulo,
      descripcion: descripcion,  
      mascota_name: mascota_name,
      name: name,
      phone: phone,
    };
    console.log(post.img)
    let $key = this.af.list(`/${ this.POSTS_A }`).push( post ).key;
    console.log($key)
    post.$key = $key;

    this.imagenes.push( post );

  }



  private mostar_toast( texto:string ){
    this.toastCtrl.create({
      message:texto,
      duration: 2500
    }).present();
  }



  //===================================================================================
  //===================================================================================
  //===================================================================================
  //===================================================================================


  cargar_adopcion( archivo:archivoSubir ){
    
    let promesa = new Promise( (resolve, reject)=>{

      this.mostar_toast("Inicio de carga");
      let storageRef = firebase.storage().ref();
      let nombreArchivo = new Date().valueOf(); //1237128371
      
      let uploadTask:firebase.storage.UploadTask =
              storageRef.child(`${ this.CARPETA_IMAGENES_EXTRAVIO  }/${ nombreArchivo }`)
              .putString( archivo.img, 'base64', { contentType: 'image/jpeg' }  );

                    
      uploadTask.on(  firebase.storage.TaskEvent.STATE_CHANGED,
          ( snapshot )=>{}, // saber el avance del archivo
          ( error )=> {  // Manejo de errores

            console.log("Error al subir ", JSON.stringify( error ));
            this.mostar_toast("Error al cargar: " + JSON.stringify( error ) );
            reject(error);

          },
          ()=>{ // Termino el proceso
            
            //let url = uploadTask.snapshot.downloadURL;
            // let url = uploadTask.snapshot.ref.getDownloadURL().then(function (URL){
            //   // console.log('File available at');
            //   // console.log(URL)
            //   return URL
            // })
            uploadTask.then((snapshot) => {
                snapshot.ref.getDownloadURL().then((url) => {
                    // do something with url here
                    //console.log(url)
                    let URL =  url
                    console.log(URL)
                    this.crear_post_adopcion( URL, 
                                      archivo.titulo, 
                                      archivo.descripcion, 
                                      archivo.mascota_name,
                                      archivo.name,
                                      archivo.phone
                                      );
                });
            });
            
            this.mostar_toast("Imagen cargada exitosamente!!");
            // this.crear_post( URL, 
            //                  archivo.titulo, 
            //                  archivo.descripcion, 
            //                  archivo.mascota_name,
            //                  archivo.name,
            //                  archivo.phone
            //                 );
            
            resolve();

          }
        )

    });



    return promesa;

  }


  private crear_post_adopcion( url:any,
                      titulo:string, 
                      descripcion:string, 
                      mascota_name:string, 
                      name:string, 
                      phone:string
                     ){
    console.log("llego hasta aqui")                    
    let post:archivoSubir = {
      img: url,
      titulo: titulo,
      descripcion: descripcion,  
      mascota_name: mascota_name,
      name: name,
      phone: phone,
    };
    console.log(post.img)
    let $key = this.af.list(`/${ this.POSTS_D }`).push( post ).key;
    console.log($key)
    post.$key = $key;

    this.imagenes.push( post );

  }


  //===================================================================================
  //===================================================================================
  //===================================================================================
  //===================================================================================


  cargar_denuncia( archivo:denuciasubir ){
    
    let promesa = new Promise( (resolve, reject)=>{

      this.mostar_toast("Inicio de carga");
      let storageRef = firebase.storage().ref();
      let nombreArchivo = new Date().valueOf(); //1237128371
      
      let uploadTask:firebase.storage.UploadTask =
              storageRef.child(`${ this.CARPETA_IMAGENES_DENUCIA  }/${ nombreArchivo }`)
              .putString( archivo.img, 'base64', { contentType: 'image/jpeg' }  );

                    
      uploadTask.on(  firebase.storage.TaskEvent.STATE_CHANGED,
          ( snapshot )=>{}, // saber el avance del archivo
          ( error )=> {  // Manejo de errores

            console.log("Error al subir ", JSON.stringify( error ));
            this.mostar_toast("Error al cargar: " + JSON.stringify( error ) );
            reject(error);

          },
          ()=>{ // Termino el proceso
            
            //let url = uploadTask.snapshot.downloadURL;
            // let url = uploadTask.snapshot.ref.getDownloadURL().then(function (URL){
            //   // console.log('File available at');
            //   // console.log(URL)
            //   return URL
            // })
            uploadTask.then((snapshot) => {
                snapshot.ref.getDownloadURL().then((url) => {
                    // do something with url here
                    //console.log(url)
                    let URL =  url
                    console.log(URL)
                    this.crear_post_denucia( URL, 
                                      archivo.titulo, 
                                      archivo.descripcion, 
                                      archivo.ubicacion,
                                      archivo.date
                                      );
                });
            });
            
            this.mostar_toast("Imagen cargada exitosamente!!");
            // this.crear_post( URL, 
            //                  archivo.titulo, 
            //                  archivo.descripcion, 
            //                  archivo.mascota_name,
            //                  archivo.name,
            //                  archivo.phone
            //                 );
            
            resolve();

          }
        )

    });



    return promesa;

  }


  private crear_post_denucia( url:any,
                      titulo:string, 
                      descripcion:string, 
                      ubicacion:string, 
                      date:string
                     ){
    console.log("llego hasta aqui")                    
    let post:denuciasubir = {
      img: url,
      titulo: titulo,
      descripcion: descripcion,  
      ubicacion: ubicacion,
      date: date,
    };
    console.log(post.img)
    let $key = this.af.list(`/${ this.POSTS_DENUCUA }`).push( post ).key;
    console.log($key)
    post.$key = $key;

    this.imagenes.push( post );

  }

}

interface denuciasubir{
  $key?:string;
  img:string;
  titulo:string;
  descripcion:string;
  ubicacion:string;
  date:string;
}

interface archivoSubir{
  $key?:string;
  img:string;
  titulo:string;
  descripcion:string;
  mascota_name:string;
  name:string;
  phone:string;
}

