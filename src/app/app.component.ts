import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  pages: any[] = [
    {
      title: 'Nueva Publicacion',
      component: 'CreatePublicationPage',
      icon:'md-add'
    },
    {
      title: 'Mis Publicaciones',
      component: 'ReadPublicationPage',
      icon:'md-list-box'
      
    },
    {
      title: 'Configuracion',
      component: 'ConfigPage',
      icon:'md-settings'
    },
    {
      title: 'Ayuda',
      component: 'HelpPage',
      icon:'md-help-circle'
    },
  ]
  rootPage:string = 'LoginPage';
  //CreatePublicationPage LoginPage
  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen,
              public alertCtrl: AlertController) {
    platform.ready().then(() => {
    statusBar.styleDefault();
    splashScreen.hide();
    });
  }


  openPage( page ) {
    if(page.component == 'CreatePublicationPage') {
      this.selectPublication( page );
    }
    else {
      this.rootPage = page.component;
    }
}
  selectPublication( page ) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Tipo de Publicacion');

    alert.addInput({
      type: 'radio',
      label: 'Adopcion',
      value: 'adopcion',
      checked: false,
    });
    alert.addInput({
      type: 'radio',
      label: 'Extravio',
      value: 'extravio',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Denuncia',
      value: 'denuncia',
      checked: false
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: tipo => {
        console.log(tipo);
        if(tipo == "extravio") {
          this.rootPage = 'CreatePublicationPage';
        }
        else if(tipo == "adopcion") {
          this.rootPage = 'CreateAdoptionPage';
        }
        else if(tipo == "denuncia") {
          this.rootPage = 'CreateDenunciationPage';
        }  
      }
    });
    alert.present();
  }
}

